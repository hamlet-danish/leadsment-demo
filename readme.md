# Leadsment API documentation

## Auth

####**POST /api/auth/login**

*Request Body*
```json
{
  "email":"admin@admin.com", //required
  "password":"admin"         //required 
}
```

*Response (OK)*
```json
{
  "user":{
    "id":1,
    "email":"admin@admin.com",
    "created_at":"2017-11-20 10:11:42",
    "updated_at":"2017-11-23 13:20:09",
    "account_id":3,
    "hasAccount":true,
    "attachedToAccount":true,
    "roles":[
      {
        "slug":"admin"
      }
    ]
  },
  "token":"eyJ0eXAiOiJKV1QiLCJhbGci...",
  "error":false
}
```
*Response (ERROR)*
```json
{
  "token":false,
  "user":false,
  "error":"Email\/Password incorrect. Please try again."
}
```

####**GET /api/auth/password/link**

Endpoint to request password reset email.

*Request Body*
```json
{
  "email":"admin@admin.com" //required
}
```

*Response (OK)*
```json
{
  "status":true,
  "error":false
}
```

*Response (ERROR)*
```json
{
  "status":false,
  "error":"We can't find a user with that e-mail address."
}
```

####**POST /api/auth/password/reset**

Endpoint to reset password.

*Request Body*
```json
{
  "email":"new1@user.com", //required
  "password":"warrior91", //required
  "password_confirmation":"warrior91", //required
  "token":"5c212358e4aad30de0061204a1077bdfcf8d92d705ed8d6e8bbbdaa7d19282e4" //required
}
```

*Response (OK)*
```json
{
  "status":true,
  "error":false
}
```

*Response (ERROR)*
```json
{
  "status":false,
  "error":"Token is required."
}
```

## Accounts

This group of endpoints is allowed for admin only.

####**GET /api/accounts**
Endpoint to get data for accounts list table.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  "sort": "email|asc", //optional, defines sort column and order
  "page": 1, //optional, page number
  "per_page": 10, //optional, number of records per page
  "filter": "test" //optional, search string
}
```

*Response (OK)*
```json
{
  "current_page":1,
  "data":[
    {
      "id":15,
      "email":"test@account.com",
      "owner_id":10,
      "name":"test",
      "created_at":"2017-11-24 08:55:13",
      "updated_at":"2017-11-24 08:59:03",
      "cases_count":0,
      "users_count":1,
      "owner":{
        "id":10,
        "email":"warrior@test.com",
        "created_at":"2017-11-23 13:08:57",
        "updated_at":"2017-11-24 08:58:59",
        "account_id":null,
        "hasAccount":true,
        "attachedToAccount":false
      }
    }
  ],
  "first_page_url":"http:\/\/demo.leadsment.com\/api\/accounts?sort=email%7Casc&filter=test&per_page=10&page=1",
  "from":1,
  "last_page":1,
  "last_page_url":"http:\/\/demo.leadsment.com\/api\/accounts?sort=email%7Casc&filter=test&per_page=10&page=1",
  "next_page_url":null,
  "path":"http:\/\/demo.leadsment.com\/api\/accounts",
  "per_page":10,
  "prev_page_url":null,
  "to":6,
  "total":6
}
```

*Response (EMPTY)*
```json
{
  "current_page":1,
  "data":[],
  "first_page_url":"http:\/\/demo.leadsment.com\/api\/accounts?sort=email%7Casc&filter=test&per_page=10&page=1",
  "from":1,
  "last_page":1,
  "last_page_url":"http:\/\/demo.leadsment.com\/api\/accounts?sort=email%7Casc&filter=test&per_page=10&page=1",
  "next_page_url":null,
  "path":"http:\/\/demo.leadsment.com\/api\/accounts",
  "per_page":10,
  "prev_page_url":null,
  "to":null,
  "total":0
}
```

####**GET /api/accounts/list**
Endpoint to get data for accounts select boxes.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  "search": "", //optional, defines sort column and order
  "exclude": "" //optional, defines account_id to exclude from list
}
```

*Response (OK)*
```json
[
  {
    "id":4,
    "email":"test4@test5.com",
    "owner_id":1,
    "name":"test4",
    "created_at":"2017-11-21 00:00:00",
    "updated_at":null,
    "owner":{
      "id":1,
      "email":"admin@admin.com",
      "created_at":"2017-11-20 10:11:42",
      "updated_at":"2017-11-23 13:20:09",
      "account_id":3,
      "hasAccount":true,
      "attachedToAccount":true
    }
  },
  {
    "id":14,
    "email":"noOwner@test.com",
    "owner_id":null,
    "name":"noOwner",
    "created_at":"2017-11-23 13:35:51",
    "updated_at":"2017-11-23 13:35:51",
    "owner":null
  }
]
```

*Response (EMPTY)*
```json
[]
```

####**GET /api/accounts/:id**
Endpoint to get data for a single account.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": "" //required, account id
}
```

*Response (OK)*
```json
{
  "status":true,
  "account":{
    "id":3,
    "email":"test3@test3.com",
    "owner_id":null,
    "name":"test3",
    "created_at":"2017-11-21 00:00:00",
    "updated_at":"2017-11-24 08:57:04",
    "owner":null,
    "users":[
      {
        "id":1,
        "email":"admin@admin.com",
        "created_at":"2017-11-20 10:11:42",
        "updated_at":"2017-11-23 13:20:09",
        "account_id":3,
        "hasAccount":true,
        "attachedToAccount":true
      },
      {
        "id":5,
        "email":"test5@admin.com",
        "created_at":"2017-11-20 10:11:42",
        "updated_at":"2017-11-23 08:57:24",
        "account_id":3,
        "hasAccount":false,
        "attachedToAccount":true
      },
      {
        "id":7,
        "email":"test7@admin.com",
        "created_at":"2017-11-20 10:11:42",
        "updated_at":"2017-11-23 08:57:24",
        "account_id":3,
        "hasAccount":false,
        "attachedToAccount":true
      }
    ]
  }
}
```

*Response (EMPTY)*
```json
{
  "status":false,
  "account":null
}
```

####**POST /api/accounts**
Endpoint to create new account instance.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Body*
```json
{
  "name":"Last", //required, account name
  "email":"last@account.com", //required, account email
  "owner":16, //optional, account owner ID
  "members":[ //optional, members IDs array
    17
  ]
}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (ERROR)*
```json
{
  "status":false,
  "errors":{
    "email":[
      "The email has already been taken."
    ]
  }
}
```

####**PUT /api/accounts/:id**
Endpoint to update account instance.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": 1 //required, account id to be updated
}
```

*Request Body*
```json
{
  "name":"Last", //required, account name
  "email":"last@account.com", //required, account email
  "owner":16, //optional, account owner ID
  "members":[ //optional, members IDs array
    17
  ]
}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (ERROR)*
```json
{
  "status":false,
  "errors":{
    "email":[
      "The email has already been taken."
    ]
  }
}
```

####**DELETE /api/accounts/:id**
Endpoint to delete account instance.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": 1 //required, account id to be deleted
}
```

*Request Body*
```json
{}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (ERROR)*
```json
{
  "status":true,
  "errors":"Account does not exist."
}
```

## Users

This endpoint group is available for admin and account owners. 
Admin is able to interact with all users, assign them to different accounts and setup as account owners.

Account owner is able to interact with users assigned to his account, he is unable to change account owners or assign users to other accounts.
Any user created by account owner is automatically assigned to his account. 

####**GET /api/users**
Endpoint to get data for user's table.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  "sort": "email|asc", //optional, defines sort column and order
  "page": 1, //optional, page number
  "per_page": 10, //optional, number of records per page
  "filter": "test" //optional, search string
}
```

*Response (OK)*
```json
{
  "current_page":1,
  "data":[
    {
      "id":4,
      "email":"test4@admin.com",
      "created_at":"2017-11-20 10:11:42",
      "updated_at":"2017-11-20 10:11:42",
      "account_id":null,
      "cases_count":0,
      "attached_to_account_count":0,
      "owned_account_count":1,
      "hasAccount":true,
      "attachedToAccount":false,
      "roles":[

      ]
    },
    {
      "id":5,
      "email":"test5@admin.com",
      "created_at":"2017-11-20 10:11:42",
      "updated_at":"2017-11-23 08:57:24",
      "account_id":3,
      "cases_count":0,
      "attached_to_account_count":1,
      "owned_account_count":0,
      "hasAccount":false,
      "attachedToAccount":true,
      "roles":[

      ]
    }
  ],
  "first_page_url":"http:\/\/demo.leadsment.com\/api\/users?sort=email%7Casc&filter=test&per_page=10&page=1",
  "from":1,
  "last_page":1,
  "last_page_url":"http:\/\/demo.leadsment.com\/api\/users?sort=email%7Casc&filter=test&per_page=10&page=1",
  "next_page_url":null,
  "path":"http:\/\/demo.leadsment.com\/api\/users",
  "per_page":10,
  "prev_page_url":null,
  "to":3,
  "total":3
}
```
*Response (EMPTY)*
```json
{
  "current_page":1,
  "data":[],
  "first_page_url":"http:\/\/demo.leadsment.com\/api\/users?sort=email%7Casc&filter=test5555&per_page=10&page=1",
  "from":null,
  "last_page":1,
  "last_page_url":"http:\/\/demo.leadsment.com\/api\/users?sort=email%7Casc&filter=test5555&per_page=10&page=1",
  "next_page_url":null,
  "path":"http:\/\/demo.leadsment.com\/api\/users",
  "per_page":10,
  "prev_page_url":null,
  "to":null,
  "total":0
}
```

####**GET /api/users/list**
Endpoint to get data for user select inputs

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  "search": "", //optional, defines sort column and order
  "exclude": "", //optional, defines account_id to exclude from list
  "userType": "free", //optional, default - "free", defines user type to fetch
  "accountID": "3" //optional allows to fetch non-free user for this account ID
}
```

*Response (OK)*
```json
[
  {
    "id":5,
    "email":"test5@admin.com",
    "created_at":"2017-11-20 10:11:42",
    "updated_at":"2017-11-23 08:57:24",
    "account_id":3,
    "hasAccount":false,
    "attachedToAccount":true
  },
  {
    "id":7,
    "email":"test7@admin.com",
    "created_at":"2017-11-20 10:11:42",
    "updated_at":"2017-11-23 08:57:24",
    "account_id":3,
    "hasAccount":false,
    "attachedToAccount":true
  }
]
```
*Response (EMPTY)*
```json
[]
```

####**POST /api/users**
Endpoint to create a user.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": "" //required, user id
}
```

*Request Body*
```json
{
  "email":"last2@user.com", //required
  "password": "", //required
  "password_confirmation": "", //required
  "account_owner":null, //optional, this field is parsed if submitted by admin only
  "account_member":17 //optional, this field is parsed if submitted by admin only
}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (Error)*
```json
{
  "status":false,
  "errors":{
    "email":[
      "The email has already been taken."
    ]
  }
}
```

####**GET /api/users/:id**
Endpoint to get data for a single user.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": "" //required, user id
}
```

*Response (OK)*
```json
{
  "status":true,
  "user":{
    "id":17,
    "email":"last2@user.com",
    "created_at":"2017-11-24 11:59:59",
    "updated_at":"2017-11-24 12:03:06",
    "account_id":17,
    "hasAccount":false,
    "attachedToAccount":true,
    "owned_account":null,
    "attached_to_account":{
      "id":17,
      "email":"last@account.com",
      "owner_id":16,
      "name":"Last",
      "created_at":"2017-11-24 12:03:06",
      "updated_at":"2017-11-24 12:03:06"
    }
  }
}
```

*Response (EMPTY)*
```json
{
  "status":false,
  "user":null
}
```

####**PUT /api/users/:id**
Endpoint to update a user.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": "" //required, user id
}
```

*Request Body*
```json
{
  "email":"last2@user.com", //required
  "password": "", //optional, may be left blank
  "password_confirmation": "", //optional, may be left blank
  "account_owner":null, //optional, this field is parsed if submitted by admin only
  "account_member":17 //optional, this field is parsed if submitted by admin only
}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (Error)*
```json
{
  "status":false,
  "errors":{
    "email":[
      "The email has already been taken."
    ]
  }
}
```

####**DELETE /api/users/:id**
Endpoint to delete a user.

*Request Headers*
```json
{
  "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGci..." //required
}
```

*Request Params*
```json
{
  ":id": "" //required, user id
}
```

*Request Body*
```json
{}
```

*Response (OK)*
```json
{
  "status":true,
  "errors":false
}
```

*Response (Error)*
```json
{
  "status":false,
  "errors": "User does not exist."
}
```