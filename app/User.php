<?php

namespace App;

use App\Notifications\PasswordResetLink;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    protected $appends = [
        'hasAccount',
        'attachedToAccount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier(){
        return $this->getKey();
    }

    public function getJWTCustomClaims(){
        return [];
    }

    public function ownedAccount(){
        return $this->hasOne('\App\Account', 'owner_id', 'id');
    }

    public function attachedToAccount(){
        return $this->belongsTo('\App\Account', 'account_id', 'id');
    }

    public function cases(){
        return $this->hasMany('\App\CaseModel');
    }

    public function getHasAccountAttribute(){
        return $this->ownedAccount()->exists();
    }

    public function getAttachedToAccountAttribute(){
        return $this->attachedToAccount()->exists();
    }

    public function sendPasswordResetNotification($token){
        $this->notify(new PasswordResetLink($token));
    }

    public function roles(){
        return $this->belongsToMany('\App\Role');
    }

    public function hasRole($role){
        return $this->roles()->where('slug', $role)->exists();
    }

    public function clearRoles(){
        $roles = Role::query()->where('slug', '!=', 'admin')->get();
        $this->roles()->detach($roles);
    }
}
