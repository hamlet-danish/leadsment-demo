<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function __construct(){
        $this->middleware('guest');
    }

    public function login(Request $request){

        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['token' => false, 'user' => false, 'error' => 'Email/Password incorrect. Please try again.']);
            }
        } catch (JWTException $e) {
            return response()->json(['token' => false, 'user' => false, 'error' => 'Cannot create token, try again later.'], 500);
        }

        return response()->json([
            'user' => User::query()->with(['roles' => function ($q){
                $q->select('slug');
            }])->find(Auth::id()),
            'token' => $token,
            'error' => false
        ]);
    }

    public function sendResetLinkEmail(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if($validator->passes()){
            $response = Password::broker()->sendResetLink(
                $request->only('email')
            );
            return $response == Password::RESET_LINK_SENT
                ? response()->json(['status' => true, 'error' => false])
                : response()->json(['status' => false, 'error' => trans($response) ]);
        }else{
            return response()->json(['status' => false, 'error' => $validator->errors()[0]]);
        }
    }

    public function resetPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        if($validator->passes()){
            $response = Password::broker()->reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function ($user, $password) {
                    $this->doResetPassword($user, $password);
                }
            );
            return $response == Password::PASSWORD_RESET
                ? response()->json(['status' => true, 'error' => false])
                : response()->json(['status' => false, 'error' => trans($response) ]);
        }else{
            return response()->json(['status' => false, 'error' => $validator->errors()[0]]);
        }
    }

    protected function doResetPassword($user, $password){
        $user->password = Hash::make($password);
        $user->setRememberToken(Str::random(60));
        $user->save();
        event(new PasswordReset($user));
    }

}
