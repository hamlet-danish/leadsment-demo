<?php

namespace App\Http\Controllers;

use App\Account;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountsController extends Controller
{
    public function index(Request $request){
        $query = app(Account::class)->newQuery()->with('owner')->withCount(['cases', 'users']);

        $sort = $request->get('sort');
        if (!empty($sort)) {
            $sorts = explode(',', request()->sort);
            foreach ($sorts as $sort) {
                list($sortCol, $sortDir) = explode('|', $sort);
                $query = $query->orderBy($sortCol, $sortDir);
            }
        } else {
            $query = $query->orderBy('id', 'asc');
        }

        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "{$request->filter}%";
                $q->where('name', 'like', $value)
                    ->orWhere('id', 'like', $value)
                    ->orWhere('email', 'like', $value)
                    ->orWhere('created_at', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int) request()->per_page : 10;
        $pagination = $query->paginate($perPage);
        $pagination->appends([
            'sort' => request()->sort,
            'filter' => request()->filter,
            'per_page' => request()->per_page
        ]);

        return response()->json($pagination);
    }

    public function getSelectList(Request $request){
        $query = Account::query()->with('owner');

        if($request->filled('search')){
            $search = $request->get('search');
            $query->where(function($q) use($search) {
                $q->where('email', 'like', '%'.$search.'%');
                $q->orWhere('name', 'like', '%'.$search.'%');
            });
        }

        if($request->filled('exclude')){
            $query->where('id', '!=', $request->get('exclude'));
        }

        return response()->json($query->limit(10)->get());
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:accounts',
            'name' => 'required|min:3'
        ]);

        if($validator->passes()){
            $account = Account::create([
                'name' => $request->get('name'),
                'email' => $request->get('email')
            ]);

            if($request->filled('owner')){
                $owner = User::find($request->get('owner'));
                $owner->clearRoles();
                $owner->roles()->attach(Role::accountRole());

                $account->owner_id = $request->get('owner');
                $account->save();
            }

            if($request->filled('members')){
                $members = $request->get('members');
                foreach ($members as $member) {
                    $m = User::find($member);
                    $m->clearRoles();

                    $account->users()->save($m);
                }
            }

            return response()->json(['status' => true, 'errors' => false]);
        }else{
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }
    }

    public function view($id){
        $account = Account::with('owner')->with('users')->find($id);

        if(empty($account)){
            return response()->json(['status' => false, 'account' => $account], 404);
        }

        return response()->json(['status' => true, 'account' => $account]);
    }

    public function update($id, Request $request){
        $account = Account::find($id);

        if(empty($account)){
            return response()->json(['status' => false, 'errors' => 'Account does not exist.']);
        }

        $validator = Validator::make($request->all(), [
            'email' => [
                'required', 'email',
                Rule::unique('accounts')->ignore($account->id)
            ],
            'name' => 'required|min:3'
        ]);

        if($validator->passes()){
            $account->name = $request->get('name');
            $account->email = $request->get('email');
            $account->save();


            $oldOwner = $account->owner()->first();
            if($request->filled('owner')){
                $account->owner_id = ($request->filled('owner')) ? $request->get('owner') : null;
                $account->owner()->first()->roles()->attach(Role::accountRole());
                $account->save();
            }else{
                if($oldOwner){
                    $oldOwner->clearRoles();
                }
                $account->owner_id = null;
                $account->save();
            }

            if($request->filled('members')){
                $ids = array();
                foreach ($request->get('members') as $m){
                    $member = User::find($m);
                    if(!$member) continue;

                    $member->account_id = $account->id;
                    $member->clearRoles();
                    $member->save();

                    $ids[] = $member->id;
                }

                $account->users()->whereNotIn('id', $ids)->update(['account_id' => null]);
            }else{
                $account->users()->update(['account_id' => null]);
            }

            return response()->json(['status' => true, 'errors' => false]);
        }else{
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id){
        $account = Account::find($id);
        if(empty($account)){
            return response()->json(['status' => false, 'errors' => 'Account does not exist.']);
        }

        $account->users()->update(['account_id' => null]);
        $account->delete();
        return response()->json(['status' => true, 'errors' => false]);
    }
}
