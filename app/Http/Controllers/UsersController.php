<?php

namespace App\Http\Controllers;

use App\Account;
use App\Mail\Welcome;
use App\Role;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    public function index(Request $request){
        $query = app(User::class)->newQuery()->with('roles')->withCount(['cases', 'attachedToAccount', 'ownedAccount']);

        if(Auth::user()->hasRole('account')){
            $query->where('account_id', Auth::user()->ownedAccount->id);
        }

        $sort = $request->get('sort');
        if (!empty($sort)) {
            $sorts = explode(',', request()->sort);
            foreach ($sorts as $sort) {
                list($sortCol, $sortDir) = explode('|', $sort);
                $query = $query->orderBy($sortCol, $sortDir);
            }
        } else {
            $query = $query->orderBy('id', 'asc');
        }

        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "{$request->filter}%";
                $q->where('id', 'like', $value)
                    ->orWhere('email', 'like', $value)
                    ->orWhere('created_at', 'like', $value);
            });
        }

        $perPage = request()->has('per_page') ? (int) request()->per_page : null;
        $pagination = $query->paginate($perPage);
        $pagination->appends([
            'sort' => request()->sort,
            'filter' => request()->filter,
            'per_page' => request()->per_page
        ]);

        return response()->json($pagination);
    }

    public function getSelectList(Request $request){
        $query = User::query();

        if($request->filled('userType')){
            $userType = $request->get('userType');
            if($userType == 'free'){
                if($request->filled('accountID')){
                    $accountID = $request->get('accountID');
                    $query->where(function($qUpper) use ($accountID){
                        $qUpper->where(function($query){
                            $query->whereDoesntHave('ownedAccount');
                            $query->whereNull('account_id');
                        });

                        $qUpper->orWhere(function($q1)  use ($accountID){
                            $q1->where(function($query) use ($accountID){
                                $query->whereHas('ownedAccount', function ($q) use ($accountID){
                                    $q->where('id', $accountID);
                                });
                            });
                            $q1->orWhere(function($query) use ($accountID){
                                $query->where('account_id', $accountID);
                            });
                        });
                    });
                }else{
                    $query->whereDoesntHave('ownedAccount');
                    $query->whereNull('account_id');
                }
            }

            if($userType == 'member'){
                $query->whereNotNull('account_id');
                $query->whereDoesntHave('ownedAccount');
            }

            if($userType == 'owner'){
                $query->whereHas('ownedAccount');
                $query->whereNull('account_id');
            }
        }

        if($request->filled('search')){
            $query->where('email', 'like', '%'.$request->get('search').'%');
        }

        if($request->filled('exclude')){
            $query->where('id', '!=', $request->get('exclude'));
        }

        return response()->json($query->limit(10)->get());
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        if($validator->passes()){
            $user = User::create([
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password'))
            ]);

            event(new Registered($user));
            Mail::to($user->email)->send(new Welcome($user, $request->get('password')));

            if(Auth::user()->hasRole('admin')){
                if($request->filled('account_member')){
                    $user->account_id = $request->get('account_member');
                    $user->save();
                } elseif($request->filled('account_owner')){
                    $account = Account::find($request->get('account_owner'));
                    if($account->owner_id){
                        $account->owner->account_id = $account->id;
                        $account->owner->save();

                        $account->owner_id = $user->id;
                        $account->save();
                    }
                }
            }else{
                $user->account_id = Auth::user()->ownedAccount->id;
                $user->save();
            }

            return response()->json(['status' => true, 'errors' => false]);
        }else{
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }
    }

    public function view($id){
        $user = User::query()->with('ownedAccount')->with('attachedToAccount');
        if(!Auth::user()->hasRole('admin')){
            $user->where('account_id', Auth::user()->ownedAccount->id);
        }

        $user = $user->find($id);
        if(empty($user)){
            return response()->json(['status' => false, 'user' => $user], 404);
        }

        return response()->json(['status' => true, 'user' => $user]);
    }

    public function update($id, Request $request){
        $user = User::find($id);
        if(empty($user)){
            return response()->json(['status' => false, 'errors' => 'User does not exist.']);
        }

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id)],
            'password' => 'min:6|confirmed'
        ]);

        if($validator->passes()){
            if(!Auth::user()->hasRole('admin') && Auth::user()->ownedAccount->id != $user->account_id){
                return response()->json(['status' => false, 'errors' => 'User does not exist.']);
            }

            $user->email = $request->get('email');

            if($request->filled('password')){
                $user->password = bcrypt($request->get('password'));
            }

            if(Auth::user()->hasRole('admin')){
                if($request->filled('account_member')){
                    if($user->hasAccount){
                        $user->ownedAccount()->update(['owner_id' => null]);
                    }
                    $user->account_id = $request->get('account_member');
                    $user->clearRoles();
                } elseif ($request->filled('account_owner')){
                    $account = Account::find($request->get('account_owner'));
                    if($account->owner_id && $account->owner_id != $user->id){
                        $account->owner->account_id = $account->id;
                        $account->owner->save();

                        $account->owner_id = $user->id;
                        $account->save();
                    }else{
                        $account->owner_id = $user->id;
                        $account->save();
                    }

                    $user->clearRoles();
                    $user->roles()->attach(Role::accountRole());
                    $user->account_id = null;
                }else{
                    if($user->hasAccount){
                        $user->ownedAccount()->update(['owner_id' => null]);
                    }
                    if($user->attachedToAccount){
                        $user->account_id = null;
                    }
                    $user->clearRoles();
                }
            }

            $user->save();

            return response()->json(['status' => true, 'errors' => false]);
        }else{
            return response()->json(['status' => false, 'errors' => $validator->errors()]);
        }
    }

    public function destroy($id){
        $user = User::find($id);
        if(empty($user)){
            return response()->json(['status' => false, 'errors' => 'User does not exist.']);
        }

        if(!Auth::user()->hasRole('admin') && Auth::user()->ownedAccount->id != $user->account_id){
            return response()->json(['status' => false, 'errors' => 'User does not exist.']);
        }

        $user->ownedAccount()->update(['owner_id' => null]);
        $user->delete();
        return response()->json(['status' => true, 'errors' => false]);
    }
}
