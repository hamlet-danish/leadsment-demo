<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseModel extends Model
{
    protected $table = 'cases';

    protected $fillable = ['account_id', 'user_id', 'title', 'description'];

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function account(){
        return $this->belongsTo('\App\Account');
    }

    public function status(){
        return $this->belongsTo('\App\Status');
    }
}
