<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['email', 'name', 'owner_id'];

    public function owner(){
        return $this->belongsTo('\App\User', 'owner_id', 'id');
    }

    public function users(){
        return $this->hasMany('\App\User');
    }

    public function cases(){
        return $this->hasMany('\App\CaseModel');
    }

}
