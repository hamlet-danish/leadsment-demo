<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $fillable = ['title', 'description'];

    public function statuses(){
        return $this->hasMany('\App\Status');
    }

    public function cases(){
        return $this->hasMany('\App\CaseModel');
    }
}
