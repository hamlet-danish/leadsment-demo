<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['stage_id', 'title', 'description'];

    public function cases(){
        return $this->hasMany('\App\CaseModel');
    }
}
