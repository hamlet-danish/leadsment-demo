<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['title', 'slug', 'description'];
    protected $hidden = ['pivot'];

    public function users(){
        return $this->belongsToMany('\App\User');
    }

    public static function adminRole(){
        return self::query()->where('slug', 'admin')->first();
    }

    public static function accountRole(){
        return self::query()->where('slug', 'account')->first();
    }
}
