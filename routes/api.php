<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthController@login');

    Route::post('password/link', 'AuthController@sendResetLinkEmail')->name('password.request');
    Route::post('password/reset', 'AuthController@resetPassword')->name('password.reset');
});

Route::prefix('accounts')->middleware(['jwt.auth', 'admin'])->group(function (){
    Route::get('/', 'AccountsController@index');
    Route::get('/list', 'AccountsController@getSelectList');
    Route::post('/', 'AccountsController@store');
    Route::get('/{id}', 'AccountsController@view');
    Route::put('/{id}', 'AccountsController@update');
    Route::delete('/{id}', 'AccountsController@destroy');
});

Route::prefix('users')->middleware(['jwt.auth', 'account'])->group(function (){
    Route::get('/', 'UsersController@index');
    Route::get('/list', 'UsersController@getSelectList');

    Route::post('/', 'UsersController@store');
    Route::get('/{id}', 'UsersController@view');
    Route::put('/{id}', 'UsersController@update');
    Route::delete('/{id}', 'UsersController@destroy');
});