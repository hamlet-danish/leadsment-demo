import axios from 'axios'
import config from '../config'

export default {
  request (method, uri, data = null) {
    if (!method) {
      console.error('API function call requires method argument')
      return
    }

    if (!uri) {
      console.error('API function call requires uri argument')
      return
    }

    let url = config.serverURI + uri
    return axios({ method, url, data, headers: {'Authorization': window.localStorage.getItem('token')} })
  }
}

axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  if (error.response.status === 401) {
    if (window.localStorage) {
      window.localStorage.setItem('user', null)
      window.localStorage.setItem('token', null)
    }

    window.location = '/login'
  }
  return Promise.reject(error.response)
})
