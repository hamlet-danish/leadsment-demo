const toastr = window.toastr
export default {
  name: 'toastMixin',
  mounted () {
    toastr.options.progressBar = true
  },
  methods: {
    showToast (message, type) {
      toastr.clear()
      switch (type) {
        case 'error':
          toastr.error(message)
          break
        case 'success':
          toastr.success(message)
          break
        case 'warning':
          toastr.warning(message)
          break
        case 'info':
          toastr.info(message)
          break
        default:
          toastr.info(message)
          break
      }
    }
  }
}
