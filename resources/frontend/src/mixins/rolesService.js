export default {
  name: 'rolesService',
  data () {
    return {
      currentUser: this.$store.state.user
    }
  },
  methods: {
    userHasRole (role) {
      return (this.currentUser.roles.filter((item) => {
        return item.slug === role
      }).length)
    }
  }
}
