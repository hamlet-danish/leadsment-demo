import api from '../api'

export default {
  name: 'accountService',
  mounted () {

  },
  methods: {
    getAccountsList (query, exclude) {
      if (typeof exclude === 'undefined') {
        exclude = ''
      }

      return api.request('get', '/accounts/list?search=' + query + '&exclude=' + exclude)
    },
    getAccount (id) {
      return api.request('get', '/accounts/' + id).catch(error => {
        console.log(error)
        this.$router.push('/accounts?accountNotFound=1')
      })
    },

    createAccount (data) {
      return api.request('post', '/accounts', data)
    },

    updateAccount (id, data) {
      return api.request('put', '/accounts/' + id, data)
    },

    deleteAccount (id) {
      return api.request('delete', '/accounts/' + id)
    }
  }
}
