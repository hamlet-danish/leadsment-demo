import api from '../api'

export default {
  name: 'usersService',
  mounted () {

  },
  methods: {
    getUsers (query, exclude, userType, accountID) {
      if (typeof exclude === 'undefined') {
        exclude = ''
      }

      if (typeof userType === 'undefined') {
        userType = 'free'
      }

      if (typeof accountID === 'undefined') {
        accountID = ''
      }

      return api.request('get', '/users/list?search=' + query + '&exclude=' + exclude + '&userType=' + userType + '&accountID=' + accountID)
    },

    getUser (id) {
      return api.request('get', '/users/' + id).catch(error => {
        console.log(error)
        this.$router.push('/users?userNotFound=1')
      })
    },

    createUser (data) {
      return api.request('post', '/users', data)
    },

    updateUser (id, data) {
      return api.request('put', '/users/' + id, data).catch(error => {
        console.log(error)
        this.$router.push('/users?userNotFound=1')
      })
    },

    deleteUser (id) {
      return api.request('delete', '/users/' + id).catch(error => {
        console.log(error)
        this.$router.push('/users?userNotFound=1')
      })
    }
  }
}
