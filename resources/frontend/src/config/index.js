export default {
  serverURI: '/api',
  fixedLayout: false,
  hideLogoOnMobile: false
}
