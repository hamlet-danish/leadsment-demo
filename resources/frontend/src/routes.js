import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import PasswordResetRequestView from './components/PasswordResetRequest.vue'
import PasswordResetFormView from './components/PasswordResetForm.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'

import AccountsListView from './components/views/AccountsList.vue'
import AccountFormView from './components/views/AccountForm.vue'
import UsersListView from './components/views/UsersList.vue'
import UserFormView from './components/views/UserForm.vue'

// Routes
const routes = [
  {
    path: '/login',
    name: 'Login',
    component: LoginView
  },
  {
    path: '/password-reset/request',
    name: 'Password Reset Request',
    component: PasswordResetRequestView
  },
  {
    path: '/password-reset/form/:email/:token',
    name: 'Password Reset Form',
    component: PasswordResetFormView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: 'Overview of environment', requiresAuth: true}
      }, {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot', requiresAuth: true}
      }, {
        path: 'accounts',
        component: AccountsListView,
        name: 'Accounts List',
        meta: {description: 'View and manage Leadsment accounts', requiresAuth: true, requiresRoles: ['admin']}
      }, {
        path: 'account/:id',
        component: AccountFormView,
        name: 'Account Settings',
        meta: {description: 'Manage account settings', requiresAuth: true, requiresRoles: ['admin']}
      }, {
        path: 'users',
        component: UsersListView,
        name: 'Users List',
        meta: {description: 'View and manage Leadsment users', requiresAuth: true, requiresRoles: ['admin', 'account']}
      }, {
        path: 'user/:id',
        component: UserFormView,
        name: 'User Settings',
        meta: {description: 'Manage user settings', requiresAuth: true, requiresRoles: ['admin', 'account']}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline', requiresAuth: true}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page', requiresAuth: true}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps', requiresAuth: true}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos', requiresAuth: true}
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
