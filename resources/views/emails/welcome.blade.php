@component('mail::message')
# Hello!
@component('mail::panel')
Welcome {{$user->email}}, your password is: <b>{{$password}}</b>.
@endcomponent
Regards,<br>{{ config('app.name') }}
@endcomponent