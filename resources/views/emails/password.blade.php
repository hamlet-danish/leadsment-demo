@component('mail::message')
# Hello
@component('mail::panel')
You are receiving this email because we received a password reset request for your account.
@component('mail::button', ['url' => $actionUrl, 'color' => 'blue'])
{{ $actionText }}
@endcomponent
@endcomponent
Regards,<br>{{ config('app.name') }}
@component('mail::subcopy')
If you’re having trouble clicking the "{{ $actionText }}" button, copy and paste the URL below
into your web browser: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endcomponent